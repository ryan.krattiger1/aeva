//=============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=============================================================================
#include "aevaAboutDialogReaction.h"

#include "aeva/mbVersion.h"

#include "pqAboutDialog.h"
#include "pqCoreUtilities.h"

#include <QLabel>
#include <QTabWidget>
#include <QTextBrowser>
#include <QTreeWidget>

//-----------------------------------------------------------------------------
aevaAboutDialogReaction::aevaAboutDialogReaction(QAction* parentObject)
  : Superclass(parentObject)
{
}

//-----------------------------------------------------------------------------
void aevaAboutDialogReaction::showAboutDialog()
{
  pqAboutDialog aboutDialog(pqCoreUtilities::mainWidget());

  // Customize the dialog for aeva
  // 1. Provide different clickable URLs.
  aboutDialog.findChild<QLabel*>("ParaViewLinkLabel")
    ->setText("<html><head/><body><p>"
              "<a href=\"https://simtk.org/projects/aeva-apps\">"
              "<span style=\"text-decoration: none; color:palette(link);\">"
              "https://simtk.org/projects/aeva-apps</a>"
              "</span>"
              "</p></body></html>");
  QString version = QString::number(aeva_VERSION_MAJOR) + '.' +
    QString::number(aeva_VERSION_MINOR) + '.' + QString::number(aeva_VERSION_PATCH);
  aboutDialog.findChild<QLabel*>("VersionLabel")->setText(QString("Version: ") + version);
  aboutDialog.findChild<QLabel*>("KitwareLinkLabel")
    ->setText("<html><head/><body><p>"
              "<span style=\"text-decoration: none; color:palette(link);\">"
              "<a href=\"https://kitware.com/\">https://kitware.com/</a>"
              "</span>"
              "</p></body></html>");

  // 2. Add a "licenses" tab for FMA license (and perhaps others)
  auto* tabs = aboutDialog.findChild<QTabWidget*>("tabWidget");
  auto* acknowledgments = new QTextBrowser;
  acknowledgments->setReadOnly(true);
  acknowledgments->setHtml(
    "<html><head/><body><p>"
    "Funding by National Institute of Biomedical Imaging and Bioengineering, "
    "National Institutes of Health (R01EB025212) awarded to "
    "Cleveland Clinic and Kitware Inc."
    "</p></body></html>");
  tabs->addTab(acknowledgments, "Acknowledgment");
  auto* licenses = new QTextBrowser;
  licenses->setReadOnly(true);
  licenses->setOpenExternalLinks(true);
  tabs->addTab(licenses, "License");
  licenses->setHtml("<html><head/><body>"
                    "<p>"
                    "This program is distributed under the "
                    "<a href=\"https://gitlab.kitware.com/aeva/aeva/-/blob/master/LICENSE.txt\">"
                    "<span style=\"text-decoration: none; color:palette(link);\">"
                    "3-clause BSD license"
                    "</span>"
                    "</a> provided with its source code. Exceptions are noted below.</p>"
                    "<hr>"

                    "<p>The Foundational Model of Anatomy ontology is distributed under the "
                    "<a href=\"http://creativecommons.org/licenses/by/3.0/\">"
                    "<span style=\"text-decoration: none; color:palette(link);\">"
                    "Creative Commons Attribution 3.0 Unported License"
                    "</span>"
                    "</a>.</p>"

                    "<p>nlohmann_json is distributed under the "
                    "<a href=\"https://github.com/nlohmann/json/blob/develop/LICENSE.MIT\">"
                    "<span style=\"text-decoration: none; color:palette(link);\">"
                    "MIT License"
                    "</span>"
                    "</a>.</p>"

                    "<p>Boost is distributed under the "
                    "<a href=\"https://www.boost.org/users/license.html\">"
                    "<span style=\"text-decoration: none; color:palette(link);\">"
                    "Boost Software License"
                    "</span>"
                    "</a>.</p>"

                    "<p>pugi is distributed under the "
                    "<a href=\"https://pugixml.org/license.html\">"
                    "<span style=\"text-decoration: none; color:palette(link);\">"
                    "MIT License"
                    "</span>"
                    "</a>.</p>"

                    "<p>PEGTL is distributed under the "
                    "<a href=\"https://github.com/taocpp/PEGTL/blob/master/LICENSE\">"
                    "<span style=\"text-decoration: none; color:palette(link);\">"
                    "MIT License"
                    "</span>"
                    "</a>.</p>"

                    "</body></html>");

  // 3. Add version numbers to the client information.
  auto* clientInfo = aboutDialog.findChild<QTreeWidget*>("ClientInformation");
  auto items = clientInfo->findItems("Version", Qt::MatchExactly, 0);
  if (!items.empty())
  {
    (*items.begin())->setText(0, "ParaView Version");
  }
  auto* aevaVersion = new QTreeWidgetItem;
  aevaVersion->setText(0, "aevaCMB Version");
  aevaVersion->setText(
    1, QString("%1.%2.%3").arg(aeva_VERSION_MAJOR).arg(aeva_VERSION_MINOR).arg(aeva_VERSION_PATCH));
  clientInfo->insertTopLevelItem(0, aevaVersion);

  aboutDialog.exec();
}
