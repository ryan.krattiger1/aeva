//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "aeva/mbMenuBuilder.h"
#include "vtkPVConfig.h"

#include "ui_mbFileMenu.h"

#include "aevaAboutDialogReaction.h"
#ifdef PARAVIEW_USE_QTHELP // Must appear after vtkPVConfig include.
#include "pqHelpReaction.h"
#endif
#include "pqDesktopServicesReaction.h"
#include "pqLoadDataReaction.h"
#include "pqLoadRestoreWindowLayoutReaction.h"
#include "pqLoadStateReaction.h"
#include "pqRecentFilesMenu.h"
#include "pqSaveDataReaction.h"
#include "pqSaveScreenshotReaction.h"
#include "pqSaveStateReaction.h"
#include "pqSetName.h"

#include <QMainWindow>
#include <QMenu>
#include <QWidget>

void mbMenuBuilder::buildFileMenu(QMenu& menu)
{
  QString objectName = menu.objectName();
  Ui::mbFileMenu ui;
  ui.setupUi(&menu);
  // since the UI file tends to change the name of the menu.
  menu.setObjectName(objectName);

  QObject::connect(
    ui.actionFileExit, SIGNAL(triggered()), QApplication::instance(), SLOT(closeAllWindows()));

  // now setup reactions.
  new pqLoadDataReaction(ui.actionFileOpen);
  new pqRecentFilesMenu(*ui.menuRecentFiles, ui.menuRecentFiles);

  new pqSaveScreenshotReaction(ui.actionFileSaveScreenshot);

  new pqSaveDataReaction(ui.actionFileSaveData);

  new pqLoadStateReaction(ui.actionFileLoadServerState);
  new pqSaveStateReaction(ui.actionFileSaveServerState);

  new pqLoadRestoreWindowLayoutReaction(true, ui.actionFileLoadWindowLayout);
  new pqLoadRestoreWindowLayoutReaction(false, ui.actionFileSaveWindowLayout);
}

void mbMenuBuilder::buildHelpMenu(QMenu& menu)
{
#ifdef PARAVIEW_USE_QTHELP
  // Help
  QAction* help = menu.addAction("Plugin, Reader, Filter, and Writer Reference")
    << pqSetName("actionHelp");
  new pqHelpReaction(help);
#endif

  // -----------------
  menu.addSeparator();

  QString manualURL = QString("https://aeva.readthedocs.io/");
  new pqDesktopServicesReaction(QUrl(manualURL),
    (menu.addAction("User's guide and tutorials") << pqSetName("actionAEVAUserGuide")));

  QString appsURL = QString("https://simtk.org/projects/aeva-apps");
  new pqDesktopServicesReaction(
    QUrl(appsURL), (menu.addAction("AEVA apps website") << pqSetName("actionAEVAapps")));

  QString sourceCodeURL = QString("https://gitlab.kitware.com/groups/aeva");
  new pqDesktopServicesReaction(
    QUrl(sourceCodeURL), (menu.addAction("Source code for aeva") << pqSetName("actionAEVAsource")));

#if !defined(__APPLE__)
  // -----------------
  menu.addSeparator();
#endif

  // See resource/mbResource.qrc for the splash image used by the dialog.
  new aevaAboutDialogReaction(menu.addAction("About...") << pqSetName("actionAbout"));
}
