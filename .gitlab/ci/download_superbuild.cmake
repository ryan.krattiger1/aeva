cmake_minimum_required(VERSION 3.12)

# In order to use a new superbuild, please copy the item from the date stamped
# directory into the `keep` directory. This ensure that new versions will not
# be removed by the uploader script and preserve them for debugging in the
# future.

set(data_host "https://data.kitware.com")

# Determine the tarball to download.
# 20210322
if ("$ENV{CMAKE_CONFIGURATION}" MATCHES "vs2019")
  set(file_item "6058fc2b2fa25629b9acecbc")
  set(file_hash "67afe2cdee1b75ea58356be1f878ca595703405e79169a4ee7f1fd00236e7056e85d12f287a4df45ff396b7794f9885ad56baf11839846c8b36556202232de4d")
elseif ("$ENV{CMAKE_CONFIGURATION}" MATCHES "macos")
  set(file_item "6058fbfd2fa25629b9acec94")
  set(file_hash "9b55bec9fee69f971207ac185ce8d43f26ce9e63c6bbb3d7d09998929ebcb58bf061e0e2883b5df33cf423cf1f28308f076ee02b72abf80a35c0d669aff35d85")
else ()
  message(FATAL_ERROR
    "Unknown build to use for the superbuild")
endif ()

# Ensure we have a hash to verify.
if (NOT DEFINED file_item OR NOT DEFINED file_hash)
  message(FATAL_ERROR
    "Unknown file and hash for the superbuild")
endif ()

# Download the file.
file(DOWNLOAD
  "${data_host}/api/v1/item/${file_item}/download"
  ".gitlab/superbuild.tar.gz"
  STATUS download_status
  EXPECTED_HASH "SHA512=${file_hash}")

# Check the download status.
list(GET download_status 0 res)
if (res)
  list(GET download_status 1 err)
  message(FATAL_ERROR
    "Failed to download superbuild.tar.gz: ${err}")
endif ()

# Extract the file.
execute_process(
  COMMAND
    "${CMAKE_COMMAND}"
    -E tar
    xf ".gitlab/superbuild.tar.gz"
  RESULT_VARIABLE res
  ERROR_VARIABLE err
  ERROR_STRIP_TRAILING_WHITESPACE)
if (res)
  message(FATAL_ERROR
    "Failed to extract superbuild.tar.gz: ${err}")
endif ()
